# Bundles from the default repo (robbyrussell's oh-my-zsh).
antigen bundle tmux
antigen bundle ssh-agent
#antigen bundle heroku
#antigen bundle lein
antigen bundle command-not-found
#antigen bundle gpg-agent
antigen bundle history
antigen bundle djui/alias-tips
#antigen bundle b4b4r07/enhancd
antigen-bundle Tarrasch/zsh-bd

#Drupal Plugins
antigen bundle webflo/drush_zsh_completion

#Tools
antigen bundle arzzen/calc.plugin.zsh

#NodeJS Plugins
antigen bundle node
antigen bundle npm

#ZSH Plugins
antigen bundle zsh-users/zsh-completions src
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle rutchkiwi/copyzshell

#Python Plugins
antigen bundle pip

#Ruby/Rails Plugins
#antigen bundle ruby
#antigen bundle rvm
#antigen bundle rails
#antigen bundle rake

#Git Plugins
antigen bundle git
antigen bundle smallhadroncollider/antigen-git-rebase
antigen bundle smallhadroncollider/antigen-git-store
antigen bundle adolfoabegg/browse-commit
#antigen bundle peterhurford/git-aliases.zsh # Breaks Austin-Desktop Rails
antigen bundle voronkovich/gitignore.plugin.zsh
antigen bundle rimraf/k

#MySQL Plugins
antigen bundle horosgrisa/mysql-colorize
antigen bundle voronkovich/mysql.plugin.zsh

# Syntax highlighting bundle.
#antigen bundle zsh-users/zsh-syntax-highlighting
